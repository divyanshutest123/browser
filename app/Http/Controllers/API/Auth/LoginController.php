<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class LoginController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Define Data

        return view('admin.dashboard.index');

        # Render
        #return response()->json( $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Void for the time being
              $this->middleware('auth');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $data = $request->all();
        if (auth()->attempt(['email' => $data['email'], 'password' => $data['password']])) {

            # create api_token if not exists, or else return the same token value to prevent invalidation of previous app token
            $user = User::find(Auth::user()->id);
            $api_token = '';
            if($user->api_token){
                $api_token = $user->api_token;
            }
            else{
                $api_token = str::random(60);
                $user->api_token = $api_token;
                $user->save();
            }
            $response = array(
                'code' => 200,
                'message'       => 'Authentication successful',
                'payload'       => array([
                    'api_token' => $api_token,
                    'user'      => $user,
                ])
            );

            return response($response, 200)
                ->header('Content-Type', 'application/json');
        }

        $response = array(
            'code' => 401,
            'message' => 'Authentication failed',
            'payload' => []
        );

        return response($response, 200)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();
        if($user){
            $user->update(['api_token' => null]);
            
            $response = array(
            'code' => 200,
            'message' => 'Successfully logged out',
            );
            return response($response, 200)
            ->header('Content-Type', 'application/json');
        }
        return response(['message' => 'Authentication failed'],401)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function socialLogin(Request $request)
    {
        $data = $request->all();
        $email = $request->get('email');
        $provider = $request->get('provider_name');
        $providerId = $request->get('provider_id');
        $user = User::where('email', $email)->where('provider_name', $provider)->where('provider_name', $providerId)->first();
        if(empty($user)) {
            $user = User::create($data);
        }

        $response = array(
                'code' => 200,
                'message' => 'Authentication successful',
                'payload' => array([
                    'api_token' => $user->api_token ?? '',
                    'user'      => $user,
                ])
            );

            return response($response, 200)
                ->header('Content-Type', 'application/json');

        $response = array(
            'code' => 401,
            'message' => 'Authentication failed',
            'payload' => []
        );

        return response($response, 200)
            ->header('Content-Type', 'application/json');
    }
}
