<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class RegisterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Define Data


        # Render
        return response()->json( $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Void for the time being
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $data = $request->all();
        if (User::where('email', $data['email'])->first()) {
            $data = array(
                'code' => 403,
                'message'=> 'User already exists',
            );

            return response()->json($data);
        }
        
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $response = array(
            'code'    => 200,
            'message' => 'User registered successful',
            'data'    =>  $user,
        );
           
        return response($response, 200)
            ->header('Content-Type', 'application/json');
    }


    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        # Void
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        # Void
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Note\UpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Http\Requests\Note\DestroyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request)
    {
       
    }
}
