<?php

namespace App\Http\Controllers\API\V1;

use App\User;
use App\Product;
use App\WishList;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    /**
     * Maybe we could bind the program into the view?
     *
     * @param Program $program
     */
    public function __construct(Product $product, WishList $wishList)
    {
        $this->product = $product;
        $this->wishList = $wishList;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # Define Data
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();
        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        
        $productData = [];
        $products = $this->product->get();
        foreach($products as $product) {
            $iswishlist = $product->wishlists->where('user_id', $user->id)->count();
            $data = new \stdClass();
            $data->id   = $product->id;
            $data->name = ($user->is_english) ? $product->name_en : $product->name_ar;
            $data->price = $product->price;
            $data->image = $product->image ?? '';
            $data->description = ($user->is_english) ? $product->description_en : $product->description_ar;
            $data->is_wishlist = $iswishlist ? true : false;
            $data->created = $product->created;
            array_push($productData, $data);
        }
        # Get the data
        $data = array(
            'code'   => '200',
            'message'=> 'success',
            'data'   => $productData,
        );
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Void for the time being
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        # Define Data
        $api_token = $request->header('TOKEN');
        $productId = $request->get('product_id');
        $user = User::where(['api_token' => $api_token])->first();
        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );
            return response()->json($data);
        }
        
        $product = $this->product->where('_id', $productId)->first();
        $data = new \stdClass();
        $data->id = $product->id;
        if($user->is_english) {
            $data->name = $product->name_en;
            $data->description = $product->description_en;
        } else {
            $data->name = $product->name_ar;
            $data->description = $product->description_ar;
        }

        $data->price = $product->price;
        $data->image = $product->image ?? '';
        $data->created = $product->created;

        # Get the data
        $data = array(
            'code'   => '200',
            'message'=> 'success',
            'data'   => $data,
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        # Void
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Note\UpdateRequest  $request
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Brand $brand)
    {
        return;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wishlist(Request $request)
    {
        # Define Data
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();
        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        
        $productData = [];
        $wishlists = $this->wishList->get();
        foreach($wishlists as $wishlist) {
            $data = new \stdClass();
            $data->id   = $wishlist->product->id;
            $data->name = ($user->is_english) ? $wishlist->product->name_en : $wishlist->product->name_ar;
            $data->price = $wishlist->product->price;
            $data->image = $wishlist->product->image ?? '';
            $data->description = ($user->is_english) ? $wishlist->product->description_en : $wishlist->product->description_ar;
            $data->created = $wishlist->product->created;
            array_push($productData, $data);
        }
        # Get the data
        $data = array(
            'code'   => '200',
            'message'=> 'success',
            'data'   => $productData,
        );
        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToWishlist(Request $request)
    {
        # Define Data
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();
        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        $data = $request->all();
        $this->wishList->create($data);
        # Get the data
        $data = array(
            'code'   => '200',
            'message'=> 'success',
        );
        return response()->json($data);
    }
}

