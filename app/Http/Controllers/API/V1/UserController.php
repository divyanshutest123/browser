<?php

namespace App\Http\Controllers\API\V1;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class UserController extends Controller
{

    /**
     * Maybe we could bind the program into the view?
     *
     * @param Program $program
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Define Data

        # Render
        return response()->json( $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Void for the time being
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        # Void
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        # Void
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Note\UpdateRequest  $request
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Brand $brand)
    {
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Http\Requests\Note\DestroyRequest  $request
     * @param  Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, Brand $brand)
    {
        return;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function setLanguage(Request $request)
    {
        $lang = $request->get('set_language');
        $setLanguage = ($lang == '0') ? 'English' : 'Arabic';
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();

        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        $data = [];
        $data['set_language'] = $lang;
        $user->update( $data );

        # Get the data
        $data = array(
            'code' => '200',
            'message'=> 'success',
            'data' => 'Set language as '.$setLanguage,
        );

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Note\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function setCurrency(Request $request)
    {
        $currency = $request->get('set_currency');
        $api_token = $request->header('TOKEN');
        $user = User::where(['api_token' => $api_token])->first();

        if(empty($user)){
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        $data = [];
        $data['set_currency'] = $currency;
        $user->update( $data );

        # Get the data
        $data = array(
            'code' => '200',
            'message'=> 'success',
            'data' => 'Set currency '.$setLanguage,
        );

        return response()->json($data);
    }

    function forgotPassword(Request $request) {
         $data = $request->all();
         $api_token = $request->header('TOKEN');
         $user = User::where('email', $data['email'])->first();
         if (empty($user)) {
            # Get the data
            $data = array(
                'code' => '404',
                'message'=> 'User not found',
            );

            return response()->json($data);
        }
        if ($user) {
            $message = 'success';
        }
         # Get the data
        $data = array(
            'code' => '200',
            'message'=> $message,
        );

        return response()->json($data);
    }
}

