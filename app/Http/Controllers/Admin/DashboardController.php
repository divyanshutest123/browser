<?php
namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user=$user;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $date = \Carbon\Carbon::today()->subDays(30);
        $users=User::latest('_id')->limit(7)->get();

        $user1 = $this->user->where('created', '>=', $date ,Carbon::now())
                                    ->where('created', '>=', $date,Carbon::now()->subDays(1))
                                    ->count();
        $user2 = $this->user->where('created', '>=', $date, Carbon::now()->subDays(1))
                                       ->where('created', '>=', $date ,Carbon::now()->subDays(2))      
                                          ->count();                                  
        $user3 = $this->user->where('created', '>=', $date, Carbon::now()->subDays(2))
                                       ->where('createt', '>=', $date ,Carbon::now()->subDays(3))     
                                          ->count(); 

        $user4 = $this->user->where('created', '>=', $date, Carbon::now()->subDays(3))
                                       ->where('created', '>=', $date, Carbon::now()->subDays(4))
                                          ->count();
                                 
        $user5 = $this->user->where('created', '>=', $date, Carbon::now()->subDays(4))
                                       ->where('created', '>=', $date, Carbon::now()->subDays(5))    
                                          ->count();

        $user6 = $this->user->where('created', '>=', $date, Carbon::now()->subDays(5))
                                       ->where('created', '>=', $date, Carbon::now()->subDays(6))
                                          ->count();
           
        

        $Mon = Carbon::now();
        $Tues = Carbon::now()->subDays(2);
        $Wed = Carbon::now()->subDays(3);
        $Thur = Carbon::now()->subDays(4);
        $Fri = Carbon::now()->subDays(5);
        $Sat = Carbon::now()->subDays(6);


        $user1mon = $this->user->where('created', '>=', $date ,Carbon::now())
                                    ->where('created', '>=', $date,Carbon::now()->subMonth(1))
                                    ->count();
        $user2mon= $this->user->where('created', '>=', $date, Carbon::now()->subMonth(1))
                                       ->where('created', '>=', $date ,Carbon::now()->subMonth(2))      
                                          ->count();                                  
        $user3mon = $this->user->where('created', '>=', $date, Carbon::now()->subMonth(2))
                                       ->where('createt', '>=', $date ,Carbon::now()->subMonth(3))     
                                          ->count(); 

        $user4mon = $this->user->where('created', '>=', $date, Carbon::now()->subMonth(3))
                                       ->where('created', '>=', $date, Carbon::now()->subMonth(4))
                                          ->count();
                                 
        $user5mon = $this->user->where('created', '>=', $date, Carbon::now()->subDays(4))
                                       ->where('created', '>=', $date, Carbon::now()->subMonth(5))    
                                          ->count();

        $user6mon = $this->user->where('created', '>=', $date, Carbon::now()->subMonth(5))
                                       ->where('created', '>=', $date, Carbon::now()->subMonth(6))
                                          ->count();
           
        

        $Jan = Carbon::now();
        $Feb = Carbon::now()->subMonth(2);
        $Mar = Carbon::now()->subMonth(3);
        $April = Carbon::now()->subMonth(4);
        $May = Carbon::now()->subMonth(5);
        $June = Carbon::now()->subMonth(6);

        return view('admin.dashboard.index', compact('users', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'Jan', 'Feb', 'Mar','April', 'May', 'June', 'user1mon', 'user2mon', 'user3mon', 'user4mon', 'user5mon', 'user6mon'));
    }
    
    
}
