@extends('layouts.app')
@section('content')

<section class="content-header">
     <div class="row">
        <div class="col-sm-12">
        <h4>Ads Management | Create Ads</h4>
         </div>
        </div>
    </section>
    <section class="content">
        <div class="container bg-w">
      <div class="row">
        <div class="col-sm-6">
            <form class="form">
            
            <ul class="add-ul d-flex" style="display: flex;">
                <li><label class="container1">Create Banner Ads
                 <input name="filter" checked="checked" type="radio">
               <span class="checkmark"></span>
               </label></li>
               <li><label class="container1">Create Browser Ads
                       <input name="filter" type="radio">
                    <span class="checkmark"></span>
                </label></li>
                 
            </ul>
            
            <div class="form-control drag">
                  <input type="file" multiple><p>Add Banner+</p>
            </div>
             <div class="cadd">
                 <label class="label"> Ad Caption</label>
                  <input type="text" class="input form-control" placeholder="Type caption here">
            </div>
             <div class="cadd">
                 <label class="label"> Ad Description</label>
                  <textarea rows="5" class="input form-control" placeholder="Type description"></textarea>
            </div>
            <div class="cadd">
                <label class="label"> Target Ads (Location)</label>
               <label class="container1">Target Ads To All
                <input name="filter" checked="checked" type="radio">
                <span class="checkmark"></span>
              </label>
              <label class="container1">Target Ads To Selected Location
                <input name="filter" type="radio">
                <span class="checkmark"></span>
              </label>
            </div>
             <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Type to add location" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-map-marker"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div>
                           <div class="cadd">
                <label class="label"> Age range (Display ad to peoples between the age )</label>
                               
 
<div id="slider-range"></div> <p>
  <input type="text" id="amount" readonly style="border:0;  color: #000000;"></p>
                  </div>
                  </div> </div>
                   <div class="col-sm-6">
                        <div class="cadd">
                           <label class="label"> Targeted Gender</label>
            
            <ul class="add-ul d-flex" style="display: flex;">
                <li><label class="container1"><i class="fa fa-male" aria-hidden="true"></i>
                 <input name="filter" checked="checked" type="radio">
               <span class="checkmark"></span>
               </label></li>
               <li><label class="container1"><i class="fa fa-female" aria-hidden="true"></i>
                       <input name="filter" type="radio">
                    <span class="checkmark"></span>
                </label></li>
                 
            </ul>
             </div>
              <div class="cadd">
                 <label class="label"> Hyper Link</label>
                  <input type="text" class="input form-control" placeholder="https://www.example.com/">
                  <p>(put the hyper link above that you want to display on viewer click)</p>
            </div>
            <div class="cadd">
                <input type="submit" class="btn btn-submit" value="Create Ad">
            </div>
             </div>
        </form>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row --></div>
    </section>

    @endsection