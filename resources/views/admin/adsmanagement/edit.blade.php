@extends('layouts.app')
   
@section('content')

    <div class="row">
    
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Ad</h2>
            </div>
            <div class="radio">
      <label><input type="radio" name="optradio" checked>Create banner ads</label>
      <label><input type="radio" name="optradio">Create browser ads</label>
    </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('advertise.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('advertise.update', $advertise->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Ad Caption:</strong>
                    <input type="text" name="caption" value="{{ $advertise->caption }}" class="form-control" placeholder="caption">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Ad Description:</strong>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Description">{{ $advertise->description }}</textarea>
                </div>
            </div>
            <div class="grid">
        <h4 style="color:black">Target Ads location</h4>
        <div class="radio">
            <label><input type="radio" name="optradio" checked>Target ads to all</label>
        </div>
       <div class="radio">
            <label><input type="radio" name="optradio">Target ads to selected location</label>
       </div>
   </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Update Ad</button>
        </div>
        </div>
   
    </form>
@endsection