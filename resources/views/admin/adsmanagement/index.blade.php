@extends('layouts.app')
@section('content')

<section class="content-header">
     <div class="row">
        <div class="col-sm-4">
            <form class="form">
                  <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Search Add By Caption" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
            </form>
        </div>
         <div class="col-sm-5">
            <ul class="add-ul">
                <li><label class="container1">All Ads
  <input name="filter" checked="checked" type="radio">
  <span class="checkmark"></span>
</label></li>
 <li><label class="container1">Banner Ads
  <input name="filter" type="radio">
  <span class="checkmark"></span>
</label></li>
 <li><label class="container1">Browser Ads
  <input name="filter" type="radio">
  <span class="checkmark"></span>
</label></li>
                 
            </ul>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('advertise.create')}}"><button class="btn btn-create-ad">
                Create Ads+
            </button></a>
        </div>
        </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                     
                  <th>s no</th>
                  <th>Ads Caption</th>
                  <th>Description</th>
                  <th>Creation Date</th>
                  <th>Actions</th>
                </tr>
                
                </thead>
                <tbody>
                 @php($no=1)  
                @foreach($advertises as $advertise)    
                  <tr class="table-danger">
                    <th scope="row">{{ $no++ }}</th>
                    <td>{{ $advertise->caption }}</td>
                    <td>{{ $advertise->description }}</td>
                    <td>{{ $advertise->created->format('m/d/Y') }}</td>  
               
                  
                  <td><a href="" class="action-link">Block</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="viewadd.html" class="action-link">View</a></td>
                
                </tr>
                @endforeach
              
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    @endsection