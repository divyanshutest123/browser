@extends('layouts.app')
@section('content')

<section class="content-header">
     <div class="row">
        <div class="col-sm-12">
        <h4>Ads Management | View</h4>
         </div>
        </div>
    </section>
    <section class="content">
        <div class="container bg-w">
      <div class="row">
        <div class="col-sm-6">
            
             <div class="cadd">
                 <label class="label"> Ad Banner</label>
                  <img src="images/c1c227bd-f301-43b6-9cf5-90d7cde2482f.png" class="img-responsive">
            </div>
             <div class="cadd">
                 <label class="label"> Ad Caption</label>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed.sng elit, sed do.</p>
            </div>
              <div class="cadd">
                 <label class="label"> Ads T&C Description</label>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed.sng elit,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed.sng elit, sed do.</p>
            </div>
            <div class="cadd">
                 <label class="label"> Ads T&C Description</label>
                  <p>02/12/2020</p>
            </div>
            </div>
                   <div class="col-sm-6">
                        <div class="row">
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>TOTAL REACH</h3>

              <p>3522</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>TOTAL VIEW</h3>

              <p>3522</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
         <!-- ./col -->
        <div class="col-lg-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>VIEWER GENDER</h3>
            </div>
           <div class="row">
        <div class="col-md-3">
          <div id="maleprogress"></div>
        </div>
        <div class="col-md-3" id="progress"><label class="container1"><i class="fa fa-male" aria-hidden="true"></i></label>
          <h2 style="color: #666666;">50%</h2>
          <p>361536</p>
        </div>
        <div class="col-md-3">
          <div id="femaleprogress"></div>
        </div>
        <div class="col-md-3" id="progress"><label class="container1"><i class="fa fa-female" aria-hidden="true"></i></label>
          <h2 style="color: #666666;">70%</h2>
          <p>361536</p>
        </div>
    </div>
            
          </div>
        </div>
        
      </div>
                      
            
             </div>
       <div class="col-sm-12">
           <div class="row bg-green">
               <div class="col-sm-6">
                    <div class="cadd">
                <label class="label"> Target Ads (Location)</label>
               <label class="container1">Target Ads To All
                <input name="filter" checked="checked" type="radio">
                <span class="checkmark"></span>
              </label>
              <label class="container1">Target Ads To Selected Location
                <input name="filter" type="radio">
                <span class="checkmark"></span>
              </label>
            </div>
             <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Type to add location" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-map-marker"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div>
               </div>
           </div>
           <div class="col-sm-6">
               <div class="cadd">
                 <label class="label"> Age range (Display ad to peoples between the age )</label>
                  
                  <p>18- 38</p>
            </div>
                 <div class="cadd">
                           <label class="label"> Targeted Gender</label>
            
            <ul class="add-ul d-flex" style="display: flex;">
                <li><label class="container1"><i class="fa fa-male" aria-hidden="true"></i>
                 <input name="filter" checked="checked" type="radio">
               <span class="checkmark"></span>
               </label></li>
               <li><label class="container1"><i class="fa fa-female" aria-hidden="true"></i>
                       <input name="filter" type="radio">
                    <span class="checkmark"></span>
                </label></li>
                 
            </ul>
             </div>
              <div class="cadd">
                 <label class="label"> Hyper Link</label>
                 
                  <p>https://www.example.com/</p>
            </div>
           </div>
       </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row --></div>
    </section>

    @endsection