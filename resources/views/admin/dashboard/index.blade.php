@extends('layouts.dashboard')
@section('content')
<section class="content-header">
     <div class="row">
        <div class="col-sm-12">
       
         </div>
        </div>
    </section>
    <section class="content">
        
        <div class="container bg-w">
         <div class="row">
           <div class="col-sm-6">
                <label class="label"> Users Activity in last 7 Days</label>
               <canvas id="myChart" width="400" height="200"></canvas>
           </div>
           <div class="col-sm-6">
                <label class="label"> Users Activity in last 6 months</label>
               <canvas id="myChart1" width="400" height="200"></canvas>
           </div>
          </div>
        </div>
        
         <div class="container bg-w">
      <div class="row">
        <div class="col-sm-6">
            <div class="doughnut-chart-container">
      <canvas id="doughnut-chartcanvas-2" width="400" height="200"></canvas>
    </div>
        </div>
        <div class="col-sm-6">
            <div class="doughnut-chart-container">
      <canvas id="doughnut-chartcanvas-1" width="400" height="200"></canvas>
    </div>
        </div>
        </div></div>
        
        
         <div class="container">
      <div class="row">
       
        <div class="col-sm-4">
            <div class=" bg-w bottom-table">
           
           <div class="row">
               <div class="col-xs-12"><label class="label"> Latest Registered Users </label><a href=""> View All</a></div>
               @php($no = 1)
               @foreach($users as $user)
               <div class="col-xs-8"> <p>{{ $user->name}}</p></div>
               <div class="col-xs-4"><a href="">View</a></div>
              @endforeach
           </div>
        
             </div>
        </div>
         <div class="col-sm-4">
            <div class=" bg-w bottom-table">
           
           <div class="row">
               <div class="col-xs-12"> <label class="label"> Subscription plan description</label></div>
         <div class="row"> 
        <div class="col-md-4">
          <div id="maleprogress"></div>
        </div>
         <div class="col-md-8">
          <span>Users, Who are using Monthly 
subscription</span><br>
<a href="#">361536</a>
        </div></div>
        <div class="row" style="margin-top:28px"> 
         <div class="col-md-4">
          <div id="femaleprogress"></div>
        </div>
         <div class="col-md-8">
          <span>Users, Who are using Monthly 
subscription</span><br>
<a href="#">361536</a>
        </div></div>
             </div>
        </div>
        </div>
        
         
               
        </div>
        </div></div>
   </div>
 </section>
  @endsection