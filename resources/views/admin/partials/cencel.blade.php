<div class="modal fade" id="modal-cancel">
  <div class="modal-dialog">
    <form action="" method="POST">
        @csrf
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Cancel</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
            <b>What are the reasons for canceling the order ?</b>
            </div>
            <div class="clearfix"></div><br><br>
            <div class="col-sm-12">
              <textarea cols="40" rows="5" name="cancelReason" maxlength="100" required></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-outline btn-primary">Submit</button>
          <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </form>  
    </div>
    <!-- /.modal-dialog -->
  </div>