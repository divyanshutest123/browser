@extends('layouts.app')
@section('content')

<section class="content-header">
     <div class="row">
        <div class="col-sm-12">
        <h4>User | View  <button class="btn-blocked">
        @if(!$user->isBlock)
                    <a href="{{ url('admin/user/block', [$user, 1])}}" class="action-link" data-placement="bottom"  data-toggle="popover" >Block</a>
                    @else
                    <a href="{{ url('admin/user/block', [$user, 0])}}" class="action-link" data-placement="bottom"  data-toggle="popover">Unblock</a>
                    @endif
        </button>
      </h4>
         </div>
        </div>
    </section>
    
     <section class="content">
        
        <div class="container">
         <div class="row">
           <div class="col-sm-4 ">
               <div class="bg-w mh-l">
                <label class="label"><i class="fa fa-user" aria-hidden="true"></i> <span> User Info</span></label>
               <p>Name: <span>{{ $user->name}}</span></p>
               <p>E mail: <span>{{ $user->email}}</span></p>
               <p>Phone: <span>{{ $user->mobile}}</span></p>
               <p>Gender: <span>Male</span></p>
               <p>Age: <span>23</span></p>
               <hr>
               <p>Looking For:</p>
               <span>Age Range:   18- 38</span>
               <span>Interested In:   Straight, Bisexual, Lesbian</span>
               <hr>
               <p>Relationship Status: <span>Single</span></p>
               <hr>
               <p>About:</p>
               <span>Lorem ipsum dolor sit amet, consectetur adipiscing 
elit, sed do eiusmod.</span>
           </div> </div>
           <div class="col-sm-8">
           <div class="row">
            <div class="col-sm-6">
            <div class="bg-w mh-m">
                <label class="label"><i class="fa fa-camera" aria-hidden="true"></i> <span> 5 photos</span></label>
               <div class="row">
                   <div class="col-sm-12">
                       <img class="limg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                   </div>
                    <div class="col-sm-12">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                   </div>
               </div>
               </div>
           </div>
            <div class="col-sm-6">
            <div class="bg-w mh-m">
                <label class="label"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> <span> User Status: 15</span> <a href="" class="action-link" style="float: right;">View All</a></label>
                
               <div class="row">
                   
                    <div class="col-sm-12">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                       <img class="simg" src="https://avatarfiles.alphacoders.com/141/141428.jpg">
                   </div>
               </div>
               </div>
           </div>
            <div class="col-sm-12">
                <div class="bg-w mh-s">
               <div class="row">
                   <div class="col-sm-6">
                       <div class="row">
                   <div class="col-sm-3">
                       <img src="{{ asset('img/94ee6c1b-8b7e-4349-8801-618cc795372e.svg')}}">
                </div>
                <div class="col-sm-9">
                       <p>User Revenue</p>
                       <span>$ 2652</span>
                </div></div>
                </div>
                 <div class="col-sm-6">
                       <div class="row">
                   <div class="col-sm-3">
                       <img src="{{ asset('img/94ee6c1b-8b7e-4349-8801-618cc795372e.svg')}}">
                </div>
                <div class="col-sm-9">
                       <p>Current Subscription Plan</p>
                       <span>$70 (Monthly)</span><span style="    color: #f82929;">Exp: 12/01/2021</span>
                </div></div>
                </div>
               </div>
            </div>      
          
          </div>
          </div>
          
          </div>
          </div>
        </div>
        </section>
    <!-- Content Header (Page header) -->
   
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table">
                <thead class="thead-dark">
                    <tr>
                     
                  <th colspan="3">Subscription History</th>
                  
                  <th><a href="" class="action-link" >View All</a></th>
                </tr>
                
                </thead>
                <tbody>
                   
                <tr>
                  <td><p>Subscription Type</p>
                  <span>Monthly</span></td>
                  <td><p>Purchase Date</p>
                  <span>Monthly</span></td>
                  <td><p>Expire Date</p>
                  <span>Monthly</span></td>
                  <td><p>Subscription Cost</p>
                  <span>Monthly</span></td>
                
                </tr>
                 <tr>
                  <td><p>Subscription Type</p>
                  <span>Monthly</span></td>
                  <td><p>Purchase Date</p>
                  <span>Monthly</span></td>
                  <td><p>Expire Date</p>
                  <span>Monthly</span></td>
                  <td><p>Subscription Cost</p>
                  <span>Monthly</span></td>
                
                </tr>
                 <tr>
                  <td><p>Subscription Type</p>
                  <span>Monthly</span></td>
                  <td><p>Purchase Date</p>
                  <span>Monthly</span></td>
                  <td><p>Expire Date</p>
                  <span>Monthly</span></td>
                  <td><p>Subscription Cost</p>
                  <span>Monthly</span></td>
                
                </tr>
                 <tr>
                  <td><p>Subscription Type</p>
                  <span>Monthly</span></td>
                  <td><p>Purchase Date</p>
                  <span>Monthly</span></td>
                  <td><p>Expire Date</p>
                  <span>Monthly</span></td>
                  <td><p>Subscription Cost</p>
                  <span>Monthly</span></td>
                
                </tr>
                
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  
    @endsection