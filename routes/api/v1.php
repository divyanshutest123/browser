<?php

/*
|--------------------------------------------------------------------------
| API V2
|--------------------------------------------------------------------------
|
| This file defines routes handling API elements
|
*/

$settings = [
    'prefix'     => 'v1',
    'namespace'  => 'API',
    'as'         => 'api.v1.',
    'middleware' => 'auth:api'
];

# login route cannot be within middleware
Route::group(Arr::except($settings,'middleware'), function($route){
    $route->post('login', 'Auth\LoginController@login');
    $route->post('social-login', 'Auth\LoginController@socialLogin');
    $route->post('signup', 'Auth\RegisterController@register');
    $route->post('logout', 'Auth\LoginController@logout');

    $route->post('forgotPassword', 'V1\UserController@forgotPassword');

    # Set language Route
    $route->post('setLanguage', 'V1\UserController@setLanguage');
    $route->post('setCurrency', 'V1\UserController@setCurrency');

    # Product Route
    $route->post('product', 'V1\ProductController@index');
    $route->post('product-detail', 'V1\ProductController@show');
    
    $route->post('wishlist', 'V1\ProductController@wishlist');
    $route->post('addToWishlist', 'V1\ProductController@addToWishlist');
});

